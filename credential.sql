CREATE OR REPLACE FUNCTION credential(text, timestamptz, text, aws.service) RETURNS text
    LANGUAGE SQL
AS
$$
SELECT concat_ws('%2F', $1, aws.amz_date($2), aws.location($3), $4, 'aws4_request');
$$;
