/* select sign_request('GET', 'storage.miatel.ru', 'oleg-bucket/sample2.bin',
    'oleg', 'ZggDQPJ0CbSbPuh8Vhzq', 'us-east-1', 604800, 's3'); */

CREATE OR REPLACE FUNCTION sign_request(_method text, _host text, _uri text, _access_key text, _secret_key text,
                                        _location text, _expire int, _service aws.service DEFAULT 's3',
                                        _headers text[] DEFAULT ARRAY []::text[],
                                        _ssl boolean DEFAULT TRUE) RETURNS text
    LANGUAGE PLpgSQL
    STABLE STRICT
AS
$_$
DECLARE
    _algorithm      CONSTANT text        := 'AWS4-HMAC-SHA256';
    _time                    timestamptz := CURRENT_TIMESTAMP;
    _ignore_headers CONSTANT text[]      := ARRAY ['authorization', 'user-agent', 'host'];
    _header                  record;
    _query                   text;
    _credential              text;
    _canonical_request       text;
    _string_to_sign          bytea;
    _signing_key             bytea;
    _signature               text;

BEGIN
    WITH
        header (key, val) AS NOT MATERIALIZED (
            SELECT lower(btrim(split_part(header, ':', 1))),
                   btrim(regexp_replace(split_part(header, ':', 2), '\s+', ' ', 'g'))
            FROM unnest(_headers) t(header)
        ),
        canonical (key, val) AS NOT MATERIALIZED (
            SELECT key, val
            FROM header
            WHERE NOT (key = ANY (_ignore_headers))
            UNION ALL
            SELECT 'host', _host
            ORDER BY 1
        )
    SELECT string_agg(canonical.key || ':' || canonical.val, E'\n') || E'\n' canonical,
           string_agg(canonical.key, ';')                                    signed
    INTO _header
    FROM canonical;

    _credential := aws.credential(_access_key, _time, _location, _service);
    _query := concat_ws('&', 'X-Amz-Algorithm=' || _algorithm,
                        'X-Amz-Credential=' || _credential,
                        'X-Amz-Date=' || aws.amz_time(_time),
                        'X-Amz-Expires=' || _expire,
                        'X-Amz-SignedHeaders=' || _header.signed);

    _canonical_request := aws.canonical_request(_method, _uri, _query, _header.canonical, _header.signed);
    _string_to_sign := aws.string_to_sign(_algorithm, _time, _location, _service, _canonical_request);
    _signing_key := aws.signing_key(_secret_key, _location, (_time AT TIME ZONE 'UTC')::date, _service);
    _signature := encode(pgcrypto.hmac(_string_to_sign, _signing_key, 'sha256'), 'hex');

    IF _ssl IS TRUE THEN
        _host := 'https://' || _host;
    ELSE
        _host := 'http://' || _host;
    END IF;

    RETURN _host || '/' || _uri || '?' || _query || '&X-Amz-Signature=' || _signature;
END
$_$;
