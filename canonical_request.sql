CREATE OR REPLACE FUNCTION canonical_request(_method text, _uri text, _query text,
                                             _header_canonical text, _header_signed text) RETURNS text
    LANGUAGE SQL
AS
$$
SELECT concat_ws(E'\n', _method, '/' || _uri, _query, _header_canonical, _header_signed, 'UNSIGNED-PAYLOAD');
$$;
