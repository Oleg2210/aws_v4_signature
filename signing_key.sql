CREATE OR REPLACE FUNCTION signing_key(text, text, date, aws.service) RETURNS bytea
    LANGUAGE PLpgSQL
AS
$_$
DECLARE
    _hmac bytea;

BEGIN
    _hmac := pgcrypto.hmac(aws.amz_date($3), 'AWS4' || $1, 'sha256');
    _hmac := pgcrypto.hmac(aws.location($2)::bytea, _hmac, 'sha256');
    _hmac := pgcrypto.hmac($4::text::bytea, _hmac, 'sha256');

    RETURN pgcrypto.hmac('aws4_request'::bytea, _hmac, 'sha256');
END
$_$;
