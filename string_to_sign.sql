CREATE OR REPLACE FUNCTION string_to_sign(text, timestamptz, text, aws.service, text) RETURNS bytea
    LANGUAGE SQL
AS
$$
SELECT concat_ws(
               E'\n', $1, aws.amz_time($2),
               concat_ws('/', aws.amz_date($2), aws.location($3), $4, 'aws4_request'),
               encode(pgcrypto.digest($5, 'sha256'), 'hex')
           )::bytea;
$$;
