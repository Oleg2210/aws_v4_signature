/*
    sudo apt-get install libssl-dev liburiparser-dev libcurl4 libcurl4-openssl-dev
    gcc byheaders.c -lssl -lcrypto -luriparser -lcurl -o byheaders
    input string s3://oleg:ZggDQPJ0CbSbPuh8Vhzq@storage.miatel.ru/oleg-bucket/sample2.bin
*/
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <curl/curl.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>
#include <uriparser/Uri.h>

#define REQUEST_METHOD "GET"
#define REGION "ru-central"
#define ALGORITHM "AWS4-HMAC-SHA256"
#define AMZ_CREDENTIAL_REQUEST "aws4_request"
#define SIGNED_HEADERS "host;x-amz-date"

#define DATE_STAMP_FORMAT "%Y%m%d"
#define AMZ_DATE_FORMAT "%Y%m%dT%H%M%SZ"

#define ARGC_MIN_VALUE 2
#define SERVICE_NAME_SIZE 8
#define ACCESS_KEY_SIZE 64
#define SECRET_KEY_SIZE 64
#define ACCESS_SECRET_SIZE ACCESS_KEY_SIZE + SECRET_KEY_SIZE
#define ACCESS_SECRET_GOOD_STATE 0
#define URL_ATTRIBUTES_GOOD_STATE 0
#define HOST_NAME_SIZE 64
#define URI_SIZE 256
#define URI_MIN_SIZE 2
#define AMZ_DATE_SIZE 17
#define AMZ_DATE_HEADER_SIZE 64
#define DATE_STAMP_SIZE 9
#define AMZ_CREDENTIAL_SIZE 128
#define CANONICAL_HEADERS_SIZE 128
#define CANONICAL_REQUEST_SIZE 512
#define HASHED_CANONICAL_REQUEST_SIZE 256
#define CREDENTIAL_SCOPE_SIZE 128
#define STRING_TO_SIGN_SIZE 256
#define HMAC_SIZE 256
#define AUTHORIZATION_HEADER_SIZE 256
#define CURL_STRING_SIZE 512
#define HASHED_PAYLOAD_SIZE 256
#define HASH_BUF_SIZE 4


enum error_codes {
	E_SUCCESS = 0,
	E_NO_INPUT_URL = 1,
	E_INPUT_URL_WRONG_FORMAT = 2,
	E_INPUT_URL_PARAMETER_EMPTY = 3,
	E_INPUT_URL_PARAMETER_OUT_OF_SIZE = 4,
	E_INPUT_URL_URI_TOO_SMALL = 5,
	E_DOWNLOAD_ERROR = 6,
	E_OUTPUT_FILE_WRONG_PATH = 7
};

typedef enum error_codes error_t;


struct DateStrings {
	char date_stamp[DATE_STAMP_SIZE];
	char amz_date[AMZ_DATE_SIZE];
};


struct Credentials {
	char service_name[SERVICE_NAME_SIZE];
	char access_key[ACCESS_KEY_SIZE];
	char secret_key[SECRET_KEY_SIZE];
	char host_name[HOST_NAME_SIZE];
	char uri[URI_SIZE];
	char output_name[URI_SIZE];
};


void save_and_check_credentials(char* buffer, size_t buffer_size, char* splitted_part, int* format_state, int* size_state){
	int is_empty = splitted_part == NULL;
	*format_state += is_empty;
	if(! is_empty){
		*size_state += strlen(splitted_part) > buffer_size;
	}
	snprintf(buffer, buffer_size, "%s", splitted_part);
}


void check_input_attribute_size(size_t allowed_size, size_t inputted_size, int* size_state){
	if (inputted_size > allowed_size){
		(*size_state)++;
	}
}


void parse_output_name(char* buffer, size_t buffer_size, char* uri){
	char uri_cpy[URI_SIZE] = {'\0'};
	snprintf(uri_cpy, URI_SIZE, "%s", uri);

	char* buf;
	buf = strtok(uri_cpy, "/");
	while(buf != NULL){
		snprintf(buffer, buffer_size, "%s", buf);
		buf = strtok(NULL, "/");
	}
}


void get_sha256_hash_string(char* buffer, size_t buffer_size, char* string_to_hash)
{
	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, string_to_hash, strlen(string_to_hash));
	SHA256_Final(hash, &sha256);

	int i = 0;
	while (i < SHA256_DIGEST_LENGTH) {
		char buf[HASH_BUF_SIZE] = {'\0'};
		snprintf(buf, HASH_BUF_SIZE, "%02x", hash[i]);
		strncat(buffer, buf, HASH_BUF_SIZE + 1);
		i++;
	}
}


void get_date_strings(struct DateStrings* date_strings)
{
	time_t utc_now = time(NULL);
	struct tm* timeinfo = gmtime(&utc_now);
	strftime((*date_strings).date_stamp, DATE_STAMP_SIZE, DATE_STAMP_FORMAT, timeinfo);
	strftime((*date_strings).amz_date, AMZ_DATE_SIZE, AMZ_DATE_FORMAT, timeinfo);
}


void get_amz_credential(char* buffer, size_t buffer_size, char* service_name, char* access_key, char* date_stamp)
{
	snprintf(buffer, buffer_size, "%s/%s/%s/%s/%s", access_key, date_stamp, REGION, service_name,
	         AMZ_CREDENTIAL_REQUEST);
}


void get_canonical_request(char* buffer, size_t buffer_size, char* host_name,char* uri, char *canonical_querystring, char* amz_date)
{
	char canonical_headers[CANONICAL_HEADERS_SIZE] = {'\0'};
	snprintf(canonical_headers, CANONICAL_HEADERS_SIZE, "%s:%s\n%s:%s\n", "host", host_name, "x-amz-date", amz_date);

	char signed_headers[] = "host;x-amz-date";
	char payload_hash[HASHED_PAYLOAD_SIZE] = {'\0'};
	get_sha256_hash_string(payload_hash, HASHED_PAYLOAD_SIZE, "");
	snprintf(buffer, buffer_size, "%s\n%s\n%s\n%s\n%s\n%s", REQUEST_METHOD, uri, canonical_querystring,
	         canonical_headers, signed_headers, payload_hash);
}


void get_credential_scope(char* buffer, size_t buffer_size, char* service_name, char* date_stamp){
	snprintf(buffer, buffer_size, "%s/%s/%s/%s", date_stamp, REGION, service_name,
	         AMZ_CREDENTIAL_REQUEST);
}


void get_string_to_sign(char* buffer, size_t buffer_size, char* credential_scope, char* amz_date, char* canonical_request)
{
	char hashed_request[HASHED_CANONICAL_REQUEST_SIZE] = {'\0'};
	get_sha256_hash_string(hashed_request, HASHED_CANONICAL_REQUEST_SIZE, canonical_request);

	snprintf(buffer, buffer_size, "%s\n%s\n%s\n%s", ALGORITHM, amz_date, credential_scope, hashed_request);
}

void get_signature(char* buffer, char* secret_key, char* service_name, char* date_stamp, char* string_to_sign)
{
	char sec_key[SECRET_KEY_SIZE] = {'\0'};
	snprintf(sec_key, SECRET_KEY_SIZE, "%s%s", "AWS4", secret_key);

	unsigned int size = 0;
	unsigned char k_date[HMAC_SIZE] = {'\0'}, k_region[HMAC_SIZE] = {'\0'}, k_service[HMAC_SIZE] = {
	    '\0'}, signing_key[HMAC_SIZE] = {'\0'}, signature[HMAC_SIZE] = {'\0'};
	HMAC(EVP_sha256(), sec_key, strlen(sec_key), (const unsigned char*)date_stamp, strlen(date_stamp), k_date, &size);
	HMAC(EVP_sha256(), k_date, size, (const unsigned char*)(REGION), strlen(REGION), k_region, &size);
	HMAC(EVP_sha256(), k_region, size, (const unsigned char*)service_name, strlen(service_name), k_service, &size);
	HMAC(EVP_sha256(), k_service, size, (const unsigned char*)(AMZ_CREDENTIAL_REQUEST), strlen(AMZ_CREDENTIAL_REQUEST),
        (unsigned char*)signing_key, &size);
	HMAC(EVP_sha256(), signing_key, size, (const unsigned char*)string_to_sign, strlen(string_to_sign), signature, &size);

	int i = 0;
	while (i < size) {
		char buf[HASH_BUF_SIZE] = {'\0'};
		snprintf(buf, HASH_BUF_SIZE, "%02hhx", signature[i]);
		strncat(buffer, buf, HASH_BUF_SIZE + 1);
		i++;
	}
}


void get_authorization_header(char* buffer, size_t buffer_size, char* access_key, char* credential_scope, char* signature){
	snprintf(buffer, buffer_size, "%s:%s %s=%s/%s, %s=%s, %s=%s", "Authorization", ALGORITHM, "Credential",
	  access_key, credential_scope, "SignedHeaders", SIGNED_HEADERS, "Signature", signature);
}


void get_amz_date_header(char* buffer, size_t buffer_size, char* amz_date){
	snprintf(buffer, buffer_size, "%s:%s", "x-amz-date", amz_date);
}


error_t download_file(char* url, char* amz_date_header, char* authorization_header, char* output_file_name){
	CURL* curl;
	struct curl_slist* headers = NULL;
	FILE* fp = fopen(output_file_name, "wb");

	if (fp == NULL){
		return E_OUTPUT_FILE_WRONG_PATH;
	}

	curl = curl_easy_init();
	headers = curl_slist_append(headers, amz_date_header);
	headers = curl_slist_append(headers, authorization_header);
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	int result = curl_easy_perform(curl);

	if (result != CURLE_OK){
		fprintf(stderr, "Download error : %s\n", curl_easy_strerror(result));
		return E_DOWNLOAD_ERROR;
	}

	fclose(fp);
	curl_easy_cleanup(curl);
	curl_slist_free_all(headers);
	curl = NULL;
	headers = NULL;
	return E_SUCCESS;
}


error_t download(struct Credentials* credentials){
	struct DateStrings date_strings;
	get_date_strings(&date_strings);

	char amz_credential[AMZ_CREDENTIAL_SIZE] = {'\0'};
	get_amz_credential(amz_credential, AMZ_CREDENTIAL_SIZE, credentials->service_name, credentials->access_key, date_strings.date_stamp);

	char canonical_querystring[] = "";

	char canonical_request[CANONICAL_REQUEST_SIZE] = {'\0'};
	get_canonical_request(canonical_request, CANONICAL_REQUEST_SIZE, credentials->host_name, credentials->uri, canonical_querystring, date_strings.amz_date);

	char credential_scope[CREDENTIAL_SCOPE_SIZE] = {'\0'};
	get_credential_scope(credential_scope, CREDENTIAL_SCOPE_SIZE, credentials->service_name, date_strings.date_stamp);

	char string_to_sign[STRING_TO_SIGN_SIZE] = {'\0'};
	get_string_to_sign(string_to_sign, STRING_TO_SIGN_SIZE, credential_scope, date_strings.amz_date, canonical_request);

	char signature[HMAC_SIZE] = {'\0'};
	get_signature(signature, credentials->secret_key, credentials->service_name, date_strings.date_stamp, string_to_sign);

	char authorization_header[AUTHORIZATION_HEADER_SIZE] = {'\0'};
	get_authorization_header(authorization_header, AUTHORIZATION_HEADER_SIZE, credentials->access_key, credential_scope, signature);

	char amz_date_header[AMZ_DATE_HEADER_SIZE] = {'\0'};
	get_amz_date_header(amz_date_header, AMZ_DATE_HEADER_SIZE, date_strings.amz_date);

	char url[CURL_STRING_SIZE] = {'\0'};
	snprintf(url, CURL_STRING_SIZE, "https://%s%s", credentials->host_name, credentials->uri);

	return download_file(url, amz_date_header, authorization_header, credentials->output_name);
}


error_t parse_input_url(UriUriA* parsed_url, struct Credentials *credentials, int parse_file_name){
	char login_and_pass[ACCESS_SECRET_SIZE] = {'\0'};
	int access_secret_state = ACCESS_SECRET_GOOD_STATE;
	int url_attributes_length_state = URL_ATTRIBUTES_GOOD_STATE;

	check_input_attribute_size(HOST_NAME_SIZE, (int)(parsed_url->hostText.afterLast - parsed_url->hostText.first),
			    &url_attributes_length_state);
	snprintf(credentials->host_name, HOST_NAME_SIZE, "%.*s", (int)(parsed_url->hostText.afterLast - parsed_url->hostText.first),
	  parsed_url->hostText.first);

	check_input_attribute_size(SERVICE_NAME_SIZE, (int)(parsed_url->scheme.afterLast - parsed_url->scheme.first),
	                           &url_attributes_length_state);
	snprintf(credentials->service_name, SERVICE_NAME_SIZE, "%.*s",(int)(parsed_url->scheme.afterLast - parsed_url->scheme.first),
	  parsed_url->scheme.first);

	check_input_attribute_size(URI_SIZE, strlen(parsed_url->hostText.afterLast), &url_attributes_length_state);
	snprintf(credentials->uri, URI_SIZE, "%s", parsed_url->hostText.afterLast);

	snprintf(login_and_pass, ACCESS_SECRET_SIZE, "%.*s", (int)(parsed_url->userInfo.afterLast - parsed_url->userInfo.first),
	  parsed_url->userInfo.first);

	save_and_check_credentials(credentials->access_key, ACCESS_KEY_SIZE, strtok(login_and_pass, ":"), &access_secret_state,
			    &url_attributes_length_state);
	save_and_check_credentials(credentials->secret_key, SECRET_KEY_SIZE, strtok(NULL, ":"), &access_secret_state,
			    &url_attributes_length_state);

	if (strlen(credentials->service_name) == 0 || strlen(credentials->host_name) == 0  ||
	    access_secret_state != ACCESS_SECRET_GOOD_STATE){
		return E_INPUT_URL_PARAMETER_EMPTY;
	}
	if (url_attributes_length_state != URL_ATTRIBUTES_GOOD_STATE){
		return E_INPUT_URL_PARAMETER_OUT_OF_SIZE;
	}
	if (strlen(credentials->uri) < URI_MIN_SIZE){
		return E_INPUT_URL_URI_TOO_SMALL;
	}

	if (parse_file_name){
		parse_output_name(credentials->output_name, URI_SIZE, credentials->uri);
	}

	return E_SUCCESS;
}


error_t get_credentials(struct Credentials* credentials, int argc, char* argv[]){
	UriUriA parsed_url;
	const char* error_pos;

	if (argc < ARGC_MIN_VALUE){
		return E_NO_INPUT_URL;
	}

	if (argc > ARGC_MIN_VALUE){
		snprintf(credentials->output_name, URI_SIZE, "%s", argv[2]);
	}

	if ( (uriParseSingleUriA(&parsed_url, argv[1], &error_pos) != URI_SUCCESS) || !( (int)(parsed_url.scheme.afterLast - parsed_url.scheme.first) &&
	(int)(parsed_url.hostText.afterLast - parsed_url.hostText.first) && (int)(parsed_url.userInfo.afterLast - parsed_url.userInfo.first)) ){
		return E_INPUT_URL_WRONG_FORMAT;
	}

	error_t error = parse_input_url(&parsed_url, credentials, (argc == ARGC_MIN_VALUE));
	if (error != E_SUCCESS){
		return error;
	}

	return E_SUCCESS;
}


int main(int argc, char* argv[])
{
	struct Credentials credentials;
	error_t error = get_credentials(&credentials, argc, argv);
	if(error != E_SUCCESS){
		return error;
	}

	return download(&credentials);
}