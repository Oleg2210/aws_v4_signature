/*
    sudo apt-get install libssl-dev
    gcc test.c -lssl -lcrypto -o test
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>


#define SERVICE_NAME "s3"
#define REQUEST_METHOD "GET"
#define HOST_NAME "storage.miatel.ru"
#define REGION "us-east-1"
#define ALGORITHM "AWS4-HMAC-SHA256"
#define AMZ_CREDENTIAL_REQUEST "aws4_request"
#define AMZ_EXPIRES "604800"

#define DATE_STAMP_FORMAT "%Y%m%d"
#define AMZ_DATE_FORMAT "%Y%m%dT%H%M%SZ"

#define AMZ_DATE_SIZE 17
#define DATE_STAMP_SIZE 9
#define AMZ_CREDENTIAL_SIZE 128
#define CANONICAL_QUERYSTRING_SIZE 256
#define CANONICAL_HEADERS_SIZE 32
#define CANONICAL_REQUEST_SIZE 512
#define HASHED_CANONICAL_REQUEST_SIZE 256
#define CREDENTIAL_SCOPE_SIZE 128
#define STRING_TO_SIGN_SIZE 256
#define HMAC_SIZE 256
#define SIGNED_URL_SIZE 512


void replace_slashes(char* str, char* buf){
    int i = 0, k = 0;
    while(str[i] != '\0'){
        if(str[i] == '/'){
            buf[k++] = '%';
            buf[k++] = '2';
            buf[k] = 'F';
        }else{
            buf[k] = str[i];
        }
        k++;
        i++;
    }
}


void get_sha256_hash_string(char* buffer, int buffer_size, char* string_to_hash){
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, string_to_hash, strlen(string_to_hash));
    SHA256_Final(hash, &sha256);

    int i = 0;
    while (i < SHA256_DIGEST_LENGTH){
        char buf[4] = {'\0'};
        snprintf(buf, 4, "%02x", hash[i]);
        strcat(buffer, buf);
        i++;
    }
}


struct DateStrings{
    char date_stamp[DATE_STAMP_SIZE];
    char amz_date[AMZ_DATE_SIZE];
};


void get_date_strings(struct DateStrings* date_strings){
    time_t utc_now = time(NULL);
    struct tm * timeinfo = gmtime(&utc_now);
    strftime((*date_strings).date_stamp, DATE_STAMP_SIZE, DATE_STAMP_FORMAT, timeinfo);
    strftime((*date_strings).amz_date, AMZ_DATE_SIZE, AMZ_DATE_FORMAT, timeinfo);
}


void get_amz_credential(char* buffer, int buffer_size, char* access_key, char* date_stamp){
    snprintf(buffer, buffer_size, "%s/%s/%s/%s/%s", access_key, date_stamp, REGION, SERVICE_NAME, AMZ_CREDENTIAL_REQUEST);
}


void get_canonical_querystring(char* buffer, char* amz_credential, char* amz_date){
    char canonical_querystring[CANONICAL_QUERYSTRING_SIZE] = {'\0'};

    char* strings_to_cat[] = {"X-Amz-Algorithm=", ALGORITHM, "&X-Amz-Credential=", amz_credential,
                            "&X-Amz-Date=", amz_date, "&X-Amz-Expires=", AMZ_EXPIRES, "&X-Amz-SignedHeaders=host"};

    int i = 0;
    while(i < sizeof(strings_to_cat)/sizeof(char*)){
        strcat(canonical_querystring, strings_to_cat[i]);
        i++;
    }

    replace_slashes(canonical_querystring, buffer);
}


void get_canonical_request(char* buffer, int buffer_size, char* uri, char* canonical_querystring){
    char canonical_headers[CANONICAL_HEADERS_SIZE] = {'\0'};
    snprintf(canonical_headers, CANONICAL_HEADERS_SIZE, "%s%s\n", "host:", HOST_NAME);

    char signed_headers[] = "host";
    char payload_hash[] = "UNSIGNED-PAYLOAD";
    snprintf(buffer, buffer_size, "%s\n%s\n%s\n%s\n%s\n%s", REQUEST_METHOD, uri, canonical_querystring,
            canonical_headers, signed_headers, payload_hash);
}


void get_string_to_sign(char* buffer, int buffer_size, char* date_stamp, char* amz_date, char* canonical_request){
    char credential_scope[CREDENTIAL_SCOPE_SIZE] = {'\0'};
    snprintf(credential_scope, CREDENTIAL_SCOPE_SIZE, "%s/%s/%s/%s", date_stamp, REGION, SERVICE_NAME, AMZ_CREDENTIAL_REQUEST);

    char hashed_request[HASHED_CANONICAL_REQUEST_SIZE] = {'\0'};
    get_sha256_hash_string(hashed_request, HASHED_CANONICAL_REQUEST_SIZE, canonical_request);

    snprintf(buffer, buffer_size, "%s\n%s\n%s\n%s", ALGORITHM, amz_date, credential_scope, hashed_request);
}

void get_signature(char* buffer, int buffer_size, char* secret_key, char* date_stamp, char* string_to_sign){
    char sec_key[64] = {'\0'};
    snprintf(sec_key, 64, "%s%s", "AWS4", secret_key);

    int size = 0;
    char k_date[HMAC_SIZE] = {'\0'}, k_region[HMAC_SIZE] = {'\0'}, k_service[HMAC_SIZE] = {'\0'}, signing_key[HMAC_SIZE] = {'\0'}, signature[HMAC_SIZE] = {'\0'};
    HMAC(EVP_sha256(), sec_key, strlen(sec_key), date_stamp, strlen(date_stamp), k_date, &size);
    HMAC(EVP_sha256(), k_date, size, REGION, strlen(REGION), k_region, &size);
    HMAC(EVP_sha256(), k_region, size, SERVICE_NAME, strlen(SERVICE_NAME), k_service, &size);
    HMAC(EVP_sha256(), k_service, size, AMZ_CREDENTIAL_REQUEST, strlen(AMZ_CREDENTIAL_REQUEST), signing_key, &size);
    HMAC(EVP_sha256(), signing_key, size, string_to_sign, strlen(string_to_sign), signature, &size);

    int i = 0;
    while (i < size){
        char buf[4] = {'\0'};
        snprintf(buf, 4, "%02hhx", signature[i]);
        strcat(buffer, buf);
        i++;
    }
}


void get_signed_url(char* buffer, int buffer_size, char* access_key, char* secret_key, char* uri){
    struct DateStrings date_strings;
    get_date_strings(&date_strings);

    char amz_credential[AMZ_CREDENTIAL_SIZE] = {'\0'};
    get_amz_credential(amz_credential, AMZ_CREDENTIAL_SIZE, access_key, date_strings.date_stamp);

    char canonical_querystring[CANONICAL_QUERYSTRING_SIZE] = {'\0'};
    get_canonical_querystring(canonical_querystring, amz_credential, date_strings.amz_date);

    char canonical_request[CANONICAL_REQUEST_SIZE] = {'\0'};
    get_canonical_request(canonical_request, CANONICAL_REQUEST_SIZE, uri, canonical_querystring);

    char string_to_sign[STRING_TO_SIGN_SIZE] = {'\0'};
    get_string_to_sign(string_to_sign, STRING_TO_SIGN_SIZE, date_strings.date_stamp, date_strings.amz_date, canonical_request);

    char signature[HMAC_SIZE] = {'\0'};
    get_signature(signature, HMAC_SIZE, secret_key, date_strings.date_stamp, string_to_sign);

    snprintf(buffer, buffer_size, "%s%s%s%s%s%s%s", "https://", HOST_NAME, uri, "?", canonical_querystring, "&X-Amz-Signature=", signature);
}


int main(){
    char access_key[] = "oleg";
    char secret_key[] = "ZggDQPJ0CbSbPuh8Vhzq";
    char uri[] = "/oleg-bucket/sample2.bin";

    char signed_url[SIGNED_URL_SIZE] = {'\0'};
    get_signed_url(signed_url, SIGNED_URL_SIZE, access_key, secret_key, uri);

    printf("%s\n", signed_url);
    return 0;
}