/*
    sudo apt-get install libssl-dev liburiparser-dev libcurl4 libcurl4-openssl-dev
	  cc bycurl.c \
	    -I ~/sources/curl/include/ -I /usr/local/include/ \
	    -L ~/sources/curl/lib/ -L /usr/local/lib/ -Wl,-rpath ~/sources/curl/lib/ \
      -lcurl -lssl -lcrypto -luriparser \
      -o bycurl
    input string s3://oleg:ZggDQPJ0CbSbPuh8Vhzq@storage.miatel.ru/oleg-bucket/sample2.bin
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <curl/curl.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>
#include <uriparser/Uri.h>

#define REGION "ru-central"
#define AWS_INITIAL_OPTIONS "aws:amz"

#define ARGC_MIN_VALUE 2
#define SERVICE_NAME_SIZE 16
#define ACCESS_SECRET_SIZE 128
#define URL_ATTRIBUTES_GOOD_STATE 0
#define HOST_NAME_SIZE 128
#define URI_SIZE 256
#define URI_MIN_SIZE 2
#define ENDPOINT_URL_SIZE 400
#define AWS_OPTIONS_SIZE 64


enum error_codes {
	E_SUCCESS = 0,
	E_NO_INPUT_URL = 1,
	E_INPUT_URL_WRONG_FORMAT = 2,
	E_INPUT_URL_PARAMETER_EMPTY = 3,
	E_INPUT_URL_PARAMETER_OUT_OF_SIZE = 4,
	E_INPUT_URL_URI_TOO_SMALL = 5,
	E_DOWNLOAD_ERROR = 6,
	E_OUTPUT_FILE_WRONG_PATH = 7
};

typedef enum error_codes error_t;


struct Credentials {
	char service_name[SERVICE_NAME_SIZE];
	char access_and_secret[ACCESS_SECRET_SIZE];
	char host_name[HOST_NAME_SIZE];
	char uri[URI_SIZE];
	char output_name[URI_SIZE];
};


void check_input_attribute_size(size_t allowed_size, size_t inputted_size, int* size_state){
	if (inputted_size > allowed_size){
		(*size_state)++;
	}
}


void parse_output_name(char* buffer, size_t buffer_size, char* uri){
	char uri_cpy[URI_SIZE] = {'\0'};
	snprintf(uri_cpy, URI_SIZE, "%s", uri);

	char* buf;
	buf = strtok(uri_cpy, "/");
	while(buf != NULL){
		snprintf(buffer, buffer_size, "%s", buf);
		buf = strtok(NULL, "/");
	}
}


error_t download_file(struct  Credentials* credentials){
	FILE* fp = fopen(credentials->output_name, "wb");
	if (fp == NULL){
		return E_OUTPUT_FILE_WRONG_PATH;
	}

	char options[AWS_OPTIONS_SIZE] = {'\0'};
	char url[ENDPOINT_URL_SIZE] = {'\0'};
	char login_options[ACCESS_SECRET_SIZE];
	snprintf(url, ENDPOINT_URL_SIZE, "https://%s%s", credentials->host_name, credentials->uri);
	snprintf(options, AWS_OPTIONS_SIZE, "%s:%s:%s", AWS_INITIAL_OPTIONS, REGION, credentials->service_name);

	CURL* curl;
	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_AWS_SIGV4, options);
	curl_easy_setopt(curl, CURLOPT_USERPWD, credentials->access_and_secret);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

	int result = curl_easy_perform(curl);
	if (result != CURLE_OK){
		fprintf(stderr, "Download error : %s\n", curl_easy_strerror(result));
		return E_DOWNLOAD_ERROR;
	}

	fclose(fp);
	curl_easy_cleanup(curl);
	curl = NULL;
	return E_SUCCESS;
}


error_t parse_input_url(UriUriA* parsed_url, struct Credentials *credentials, int parse_file_name){
	int url_attributes_length_state = URL_ATTRIBUTES_GOOD_STATE;

	check_input_attribute_size(HOST_NAME_SIZE, (int)(parsed_url->hostText.afterLast - parsed_url->hostText.first),
	                           &url_attributes_length_state);
	snprintf(credentials->host_name, HOST_NAME_SIZE, "%.*s", (int)(parsed_url->hostText.afterLast - parsed_url->hostText.first),
	         parsed_url->hostText.first);

	check_input_attribute_size(SERVICE_NAME_SIZE, (int)(parsed_url->scheme.afterLast - parsed_url->scheme.first),
	                           &url_attributes_length_state);
	snprintf(credentials->service_name, SERVICE_NAME_SIZE, "%.*s",(int)(parsed_url->scheme.afterLast - parsed_url->scheme.first),
	         parsed_url->scheme.first);

	check_input_attribute_size(URI_SIZE, strlen(parsed_url->hostText.afterLast), &url_attributes_length_state);
	snprintf(credentials->uri, URI_SIZE, "%s", parsed_url->hostText.afterLast);

	check_input_attribute_size(ACCESS_SECRET_SIZE, (int)(parsed_url->userInfo.afterLast - parsed_url->userInfo.first),
						       &url_attributes_length_state);
	snprintf(credentials->access_and_secret, ACCESS_SECRET_SIZE, "%.*s", (int)(parsed_url->userInfo.afterLast - parsed_url->userInfo.first),
	         parsed_url->userInfo.first);

	if (strlen(credentials->service_name) == 0 || strlen(credentials->host_name) == 0 ||
	strlen(credentials->service_name) == 0){
		return E_INPUT_URL_PARAMETER_EMPTY;
	}
	if (url_attributes_length_state != URL_ATTRIBUTES_GOOD_STATE){
		return E_INPUT_URL_PARAMETER_OUT_OF_SIZE;
	}
	if (strlen(credentials->uri) < URI_MIN_SIZE){
		return E_INPUT_URL_URI_TOO_SMALL;
	}

	if (parse_file_name){
		parse_output_name(credentials->output_name, URI_SIZE, credentials->uri);
	}

	return E_SUCCESS;
}


error_t get_credentials(struct Credentials* credentials, int argc, char* argv[]){
	UriUriA parsed_url;
	const char* error_pos;

	if (argc < ARGC_MIN_VALUE){
		return E_NO_INPUT_URL;
	}

	if (argc > ARGC_MIN_VALUE){
		snprintf(credentials->output_name, URI_SIZE, "%s", argv[2]);
	}

	if ( (uriParseSingleUriA(&parsed_url, argv[1], &error_pos) != URI_SUCCESS) || !( (int)(parsed_url.scheme.afterLast - parsed_url.scheme.first) &&
	                                                                                 (int)(parsed_url.hostText.afterLast - parsed_url.hostText.first) && (int)(parsed_url.userInfo.afterLast - parsed_url.userInfo.first)) ){
		return E_INPUT_URL_WRONG_FORMAT;
	}

	error_t error = parse_input_url(&parsed_url, credentials, (argc == ARGC_MIN_VALUE));
	if (error != E_SUCCESS){
		return error;
	}

	return E_SUCCESS;
}


int main(int argc, char* argv[])
{
	struct Credentials credentials;
	error_t error = get_credentials(&credentials, argc, argv);
	if(error != E_SUCCESS){
		return error;
	}

	return download_file(&credentials);
}