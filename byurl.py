import sys, os, base64, datetime, hashlib, hmac
import requests
import urllib.parse


def sign(key, msg):
    return hmac.new(key, msg.encode('utf-8'), hashlib.sha256).digest()


def getSignatureKey(key, dateStamp, regionName, serviceName):
    kDate = sign(('AWS4' + key).encode('utf-8'), dateStamp)
    kRegion = sign(kDate, regionName)
    kService = sign(kRegion, serviceName)
    kSigning = sign(kService, 'aws4_request')
    return kSigning


access_key = 'oleg'
secret_key = 'ZggDQPJ0CbSbPuh8Vhzq'
service = 's3'
method = 'GET'
host = 'storage.miatel.ru'
region = 'us-east-1'
algorithm = 'AWS4-HMAC-SHA256'

t = datetime.datetime.utcnow()
amzdate = t.strftime('%Y%m%dT%H%M%SZ')
datestamp = t.strftime('%Y%m%d')
amzdate = "20210105T155627Z"
datestamp = "20210105"

request_parameters = ''
canonical_uri = 'oleg-bucket/sample2.bin'
canonical_headers = 'host:' + host + '\n'
signed_headers = 'host'
payload_hash = 'UNSIGNED-PAYLOAD'

amz_credential = '/'.join([access_key, datestamp, region, service, 'aws4_request'])
canonical_querystring = {
    'X-Amz-Algorithm': algorithm,
    'X-Amz-Credential': amz_credential,
    'X-Amz-Date': amzdate,
    'X-Amz-Expires': 604800,
    'X-Amz-SignedHeaders': 'host'
}

canonical_querystring = urllib.parse.urlencode(canonical_querystring)
canonical_request = method + '\n' + f'/{canonical_uri}' + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash
credential_scope = datestamp + '/' + region + '/' + service + '/' + 'aws4_request'
string_to_sign = algorithm + '\n' + amzdate + '\n' +  credential_scope + '\n' +  hashlib.sha256(canonical_request.encode('utf-8')).hexdigest()
signing_key = getSignatureKey(secret_key, datestamp, region, service)
signature = hmac.new(signing_key, (string_to_sign).encode('utf-8'), hashlib.sha256).hexdigest()
url = f'https://{host}/{canonical_uri}?{canonical_querystring}&X-Amz-Signature={signature}'
print(url)