DROP SCHEMA IF EXISTS aws CASCADE;
CREATE SCHEMA aws;
SET search_path = aws, public;

GRANT USAGE ON SCHEMA aws TO PUBLIC;

ALTER DEFAULT PRIVILEGES IN SCHEMA aws GRANT EXECUTE ON FUNCTIONS TO PUBLIC;

CREATE TYPE service AS enum ('s3');

\ir 'date.sql'
\ir 'location.sql'

\ir 'canonical_request.sql'
\ir 'credential.sql'
\ir 'signing_key.sql'
\ir 'string_to_sign.sql'

\ir 'sign_request.sql'
